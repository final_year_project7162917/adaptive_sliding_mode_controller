# Adaptive Sliding Mode Controller
Adaptive sliding mode controller developed as a Final Year Project for a Masters in Integrated Mechanical and Electrical Engineering. See "final_year_report.pdf".

All Simulink models were created using MATLAB 2022b.

The controller architecture is shown below.
![Controller Architecture](Figures/experimental_arch.PNG)

## SMC_simulation.slx
Simulation variant of the controller. Note: the actuator space controller shown in the controller architecture is not used in the simulation.

## SMC_experimental.slx
Experimental variant of the controller using Speedgoat for Data Acquisition & Control. 

## SMC_experimental_2019b.slx
Experimental variant ported for MATLAB 2019b.

## SMC_experimental_neat.slx
Neat version of SMC_experimental.slx for increased clarity. Non-functional.
